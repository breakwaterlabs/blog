---
date: 2023-08-22T18:00:00-00:00
description: "Making RBAC, Linux, and Auditing easy with PowerShell"
tags: ["rbac", "active directory", "powershell", "linux", "ssh"]
title: "Secure by Design: Active Directory Architecture"
draft: false
---

## The Challenge of RBAC in AD
Let me know if any of these sound familiar (possible trigger warning):

 1. "Joe is taking Bill's position on Monday. Please add his account to all 89 security groups that Bill was a member of."
 1. "Requirements: In order to function, the robovac requires Domain Admin rights at all times..."
 1. "We applied the STIG GPOs and everything blew up, so we blocked inheritance and clicked 'enforce'..."
 1. "For some reason our stack of 387 GPOs broke when someone made a trivial change..."
 1. *Angry noises because 2 of your systems don't have you in sudoers and authorized_keys*

Out of the box, Active Directory provides some powerful tools for securing a network via DACLs, nested group memberships, and GPOs. But there are very few actual guiderails to assist in using those tools in a cohesive, sensible way. Because of this, many organizations turn to third party products (BeyondTrust, Quest, CyberArk, Okta) to implement features that AD carries out of the box; and this leads to additional complexity and risk.

To add to the problem, some compliance frameworks mandate that domain admins may or shall not be used by standard accounts. STIG in particular [mandates "Deny Logon" for domain admins on non-Domain Controllers](https://www.stigviewer.com/stig/windows_10/2020-06-15/finding/V-63871), which can leave many admins scratching their heads as to how to implment the policy while still having the access they need to do their job as a systems administrator.

Additionally, while Linux, OpenSSH, sudo, and SSSD support deep integration into Active Directory, most organizations do not use these integrations and instead either rely on CM tools like Ansible to manage access policy or manually configure access (e.g. by `visudo` or `ssh-copy-id`)

I believe that an opinionated, heirarchical, and template-driven approach to Active Directory structure can solve many of these issues, and that Linux can be a first-class citizen on an AD network. To that end I have created a powershell module that implements this approach.

## General Design
In general, most organizations' directories can be broken up into their different business units (e.g. Accounting, Software Dev, Infrastructure), and the systems or projects that they own.

Ideally, each business unit answers to one or a few individuals who have authority over their systems / projects, and in turn each system/project is owned by one/a few individuals. In a RACI matrix, these might be "Accountable", and conceptually have full authority over their area.

There are also typically groups of users who are delegated the the responsibility to perform administrative activities in their area. They might be termed "Operators", and generally need a high but limited level of access-- sufficient to perform their job, but insufficient to bypass controls.

Finally there are typically groups of users who need application-level access to an area, but not access to make any changes.

With this general framework, we can begin to formalize the design.

### Key Terms
I implement this framework as follows

 * **Orgs**: Business units or groups. Each one should have an owner who is the 'Responsible' in the RACI matrix, and should be the decision-maker for all systems that fall under their purview
 * **Components**: Projects, short-term engagements, or software stacks. These all share a common lifecycle, a direct owner, and logical access, and are owned by an Org. For instance, "_VMWare vSphere_" or "_ADFS_" or "_Red Team 2023_" or "_GitLab_" would be possible components. All components belong to an org.

> NB: Some organizations may benefit from a level above "Orgs"-- a "tenant" concept that could be used for much larger organizations. This is compatible with the design below and will likely be added to the design in the future, but I believe these organizations are more likely to have a mature RBAC approach and my focus is on the other 95% of organizations where 2 levels are sufficient. 

Within these business heirarchies fall several other concepts:

 * **Endpoints**: Computer objects in AD. Systems that are managed by and authenticate against the directory. All endpoints should belong to a component.
 * **Rights**: Domain-local security groups that are referenced in access policies, permissions, or access lists. Rights exist at a global, org, and component level.
 * **Roles**: Global security groups are members of "Rights" groups, and which contain Security principals like Users. Roles exist at a global, org, and component level.
 * ServiceAccounts: Managed Service Accounts, and user objects for legacy systems that do not support MSAs

When we adhere to these principals, creating security groups, GPOs, and DACLs starts to become easier. Anyone who is a 'global admin' should have a certain level of access to all endpoints, while a component admin should only have those rights within their component.

### Object Naming
As a principle, it is better to be clear than to be overly concise in naming objects because documentation is hard. Objects names should indicate clearly what they are. For security groups, their names should specify what org or component they belong to:

    [Type]-[Org]-[Component]-[WhatItIs]
Here are some example names that an organization might have:
 * Right-Global-AddEndpoint
 * Right-Infrastructure-LocalAdmin
 * Right-Infrastructure-vSphere-LocalAdmin
 * Role-Infrastructure-Operator

Other objects like computernames, users, and service accounts have more strict length limitations should simply clearly identify them.

### OU Structure and Delegation

When implemented, the OU structure might look like this:

 * Orgs _(Global level)_
   * Infrastructure _(Org level)_
     * Rights
     * Roles
     * Components
       * VMWare _(Component Level)_
         * Endpoints
         * Rights
         * Roles
         * ServiceAccounts
       * Email
         ....
   * SoftwareDevelopers
    ....

This creates some natural points to link policies and delegate permissions: 
 * **Domain Root**: Only compliance-related policies should be linked here (e.g. STIG).
 * **Global level**: GPOs affecting domain systems can be linked here, such as general PKI settings, organization-specific logon banners, and so on. Global roles should have permissions granted here 
 * **Org Level**: GPOs affecting a business unit, and permissions for org-level roles. 
 * **Component Level**: GPOs affecting only a single component.

### Default Rights
Without knowing much about an organization, we can infer some common access groups that make sense at any particular level in our heirarchy:

| Name | Description |
| ------ | ------ |
| App-Admin | Full rights inside the Component's application interface (e.g. ESXi Admin) |
| AddEndpoint | Allowed to create / join computer objects to the domain (or component) |
| GPOEdit | Edit rights on All GPOs |
| Rights-Admin | Create and delete new rights, and modify membership of all groups. |
| Roles-Manage | Create and delete new roles, and modify membership of roles. |
| LocalAdmin | Local admin rights on Windows hosts |
| sudo_full | Full sudo permissions on Linux hosts |
| sudo_operate | Permissions to start/stop processed and services, but cannot change /etc |
| BatchLogon | Rights for batch logon / scheduled task / cron access |
| ServiceLogon | Rights for logon as service in this Org |
| RemoteLogon | Rights to use Remote Desktop / SSH / Cockpit in this Org |

### Default Roles
Likewise, we can infer some general roles that make sense: 

| Role | Description |
| ------ | ------ |
| Owner | Full rights to modify role membership, access and administer endpoints, and update GPOs |
| Operator | Limited admin permissions, and permissions to audit / edit GPOs |
| User | Permissions to connect to and use application | 

### GPO policy
When a new "component" is created, a good RBAC structure should ensure that the rights we have inferred "work" (that is: that "remoteLogon" actually lets you log on). It also needs to account for the operational reality that software systems sometimes require deviations from the standard: they may require limited use of NTLMv1 for instance, and the organization may accept that risk. There should be a way to allow a Component or Org owner to create that deviation without affecting others and without creating an administrative nightmare.

At each level of delegation therefore, we can create two GPOs:

 * _HBAC-[OrgName]-[ComponentName]: Only editable by domain admins. Sets the "allow logon as service / Remote / Batch" permissions, and adds the "localAdmins" security groups to the endpoint's local Administrators group
 * _Settings-[OrgName]-[ComponentName]: Editable by the 'GPOEdit' right. Blank by default, but usable for scoped deviations from global policy

We enforce that the _HBAC GPOs have link order 1, and we do not use the 'block inheritance' or 'enforced' GPO options as they are anti-patterns stemming from poor design.

## Linux integration
SSSD has for many years supported several little-used integrations with Active Directory. 

### Sudo
By activating the 'sudo' provider and applying a minor [schema update](https://github.com/lbt/sudo/blob/master/doc/schema.ActiveDirectory), SSSD will fetch ['sudoRole' objects](https://www.sudo.ws/docs/man/1.8.17/sudoers.ldap.man/) from LDAP that specify a user, the commands they may run, and the hosts to which the rule applies. 
> Note that sudo does not understand the concept of an OU, so instead nisNetgroups can be used to group together computer objects.

### HBAC / Access Control
It is common to see SSSD configured with 'simple' access control, specifing what groups are permitted to authenticate on a host. However, SSSD has for a long time supported [using GPOs for access control](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/windows_integration_guide/sssd-gpo) via e.g. the "Allow/Deny logon as [Service | Batch | Remote]" entries and mapping those to Linux concepts (SSH, Cockpit, cron). This requires a one-line change to sssd.conf, and a supporting RBAC structure.

### sshPublicKeys
Populating a system's authorized_keys is the most common way to associate a user with their public key, but SSSD supports fetching the public key on the fly from a named LDAP attribute. A common [schema mod](https://www.saotn.org/retrieve-ssh-public-key-from-active-directory-for-ssh-authentication/) adds SSSD's default attribute (sshPublicKey) to Active Directory, but this is very manual and many admins are understandably reticent to apply such a change to AD by hand.

### SSH GSSAPI
Some compliance documents (e.g. STIG) [indicate that GSSAPI should only be used where necessary](https://www.stigviewer.com/stig/oracle_linux_5/2018-10-03/finding/V-22473), which has led to many organizations opting to use public keys instead of leveraging kerberos. This is due to a misunderstanding of both the policy and the risks; kerberos can be a risk if forwardable tickets (unconstrained delegation) is used, but in general GSSAPI is safer than public keys because it is much easier to manage, is zero-trust, and supports mutual authentication via gssapi-keyex. This should be used where possible both to allow SSO SSH, and to avoid the [issues with the TOFU trust model](https://en.wikipedia.org/wiki/Trust_on_first_use#Model_strengths_and_weaknesses). 
